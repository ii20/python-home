from pyhap.accessory import Accessory
from pyhap.const import CATEGORY_SWITCH

class Switch(Accessory):
  category = CATEGORY_SWITCH

  def __init__(self, *args, **kwargs):
    super().__init__(*args)
    print("Homekit device " + args[1])

    self.service = self.add_preload_service('Switch', chars=['On'])
    self.on_char = self.service.get_characteristic('On')

    self.service.configure_char('On', setter_callback=self.setSwitch)

    self.device = kwargs.get('devices')[0]
    self.device.addCallback(self.setState)

  def setSwitch(self, value):
    if value:
      self.device.publish('ACTIVE')
    else:
      self.device.publish('OFF')

  def setState(self):
    value = str(self.device.value)

    if value != 'OFF':
      self.on_char.set_value(1)
    else:
      self.on_char.set_value(0)
