from pyhap.accessory import Accessory
from pyhap.const import CATEGORY_SENSOR

class TemperatureSensor(Accessory):
  category = CATEGORY_SENSOR

  def __init__(self, *args, **kwargs):
    super().__init__(*args)
    print("Homekit device " + args[1])
    
    self.char = self.add_preload_service('TemperatureSensor')\
                    .configure_char('CurrentTemperature')
    
    self.device = kwargs.get('devices')[0]
    self.device.addCallback(self.set_state)

  def set_state(self):
    value = round(float(self.device.value))
    self.char.set_value(value)
  