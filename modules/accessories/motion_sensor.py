from threading import Timer

from pyhap.accessory import Accessory
from pyhap.const import CATEGORY_SENSOR

class MotionSensor(Accessory):
  category = CATEGORY_SENSOR

  def __init__(self, *args, **kwargs):
    super().__init__(*args)

    serv_motion = self.add_preload_service('MotionSensor')
    self.char_detected = serv_motion.configure_char('MotionDetected')

    self.device = kwargs.get('devices')[0]
    self.device.addCallback(self.trigger)

    self.timer = Timer(0, None)

  def trigger(self):
    self.char_detected.set_value(True)
    self.timer = Timer(4.0, self.reset)
    self.timer.start()

  def reset(self):
    self.char_detected.set_value(False)

