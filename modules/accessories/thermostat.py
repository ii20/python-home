from pyhap.accessory import Accessory
from pyhap.const import CATEGORY_THERMOSTAT

class Thermostat(Accessory):
  category = CATEGORY_THERMOSTAT

  def __init__(self, *args, **kwargs):
    super().__init__(*args)
    print("Homekit device " + args[1])

    self.service = self.add_preload_service('Thermostat', chars=["CurrentHeatingCoolingState",
                                                                  "TargetHeatingCoolingState",
                                                                  "CurrentTemperature",
                                                                  "TargetTemperature",
                                                                  "TemperatureDisplayUnits"])

    self.service.configure_char('TemperatureDisplayUnits', value=0)
    self.service.configure_char('TargetHeatingCoolingState', value=3)
    self.temperature_char = self.service.get_characteristic('TargetTemperature')

    self.service.configure_char('TargetTemperature', setter_callback=self.setTemperature)

    self.read_device = kwargs.get('devices')[0]
    self.read_device.addCallback(self.set_state)

  def setTemperature(self, value):
    self.read_device.publish(self.round_to_halfs(value))

  def set_state(self):
    value = float(self.read_device.value)
    self.temperature_char.set_value(value)

  def round_to_halfs(self, value):
    return round(value * 2) / 2
