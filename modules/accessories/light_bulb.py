from pyhap.accessory import Accessory
from pyhap.const import CATEGORY_LIGHTBULB

class LightBulb(Accessory):
  category = CATEGORY_LIGHTBULB

  def __init__(self, *args, **kwargs):
    super().__init__(*args)
    print("Homekit device " + args[1])
    
    self.service = self.add_preload_service('Lightbulb', chars=['On', 'Brightness', 'ColorTemperature'])
    self.on_char = self.service.get_characteristic('On')
    self.brightness_char = self.service.get_characteristic('Brightness')

    self.service.configure_char('On', setter_callback=self.set_lights)
    self.service.configure_char('Brightness', setter_callback=self.set_brightness)
    self.service.configure_char('ColorTemperature', setter_callback=self.set_color_temperature)

    self.read_device = kwargs.get('devices')[0]
    self.write_device = kwargs.get('devices')[1]

    self.read_device.addCallback(self.set_state)

  # update phone > home
  def set_lights(self, value):
    print("set light to " + str(value))
    mqtt.publish(self.write_device.input_topic, value)

  def set_brightness(self, value):
    print("set brightness to " + str(value))
    # mqtt.publish(self.write_device.input_topic, value)

  def set_color_temperature(self, value):
    print("set color_temperature to " + str(value))
    # mqtt.publish(self.write_device.input_topic, value)

  # update home > phone
  def set_state(self):
    brightness = int(self.read_device.value)
    
    self.on_char.set_value(brightness > 0)
    self.brightness_char.set_value(brightness)
