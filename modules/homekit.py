import logging
import signal

from modules.device import Device
from modules.accessories.humidity_sensor import HumiditySensor
from modules.accessories.temperature_sensor import TemperatureSensor
from modules.accessories.switch import Switch
from modules.accessories.thermostat import Thermostat
from modules.accessories.motion_sensor import MotionSensor

from pyhap.accessory import Bridge
from pyhap.accessory_driver import AccessoryDriver

logging.basicConfig(level=logging.INFO)

class Homekit():
	def __init__(self):
		self.driver = AccessoryDriver(port=51826)
		self.bridge = Bridge(self.driver, "Bridge")

		for location in ['hallway', 'kitchen', 'bedroom']:
			device = Device("home/"+location+"/wallswitch/humidity")
			self.register_accessory(HumiditySensor, "Humidity" + location, [device])

			device = Device("home/"+location+"/wallswitch/temperature")
			self.register_accessory(TemperatureSensor, "Temperature" + location, [device])

			device = Device("home/"+location+"/wallswitch/pir")
			self.register_accessory(MotionSensor, "Motion" + location, [device])

		for location in locations.withLighting():
			device = Device("home/"+location.key+"/mode")
			self.register_accessory(Switch, "Lights" + location.key, [device])

		device = Device("home/livingroom/thermostat/temperature")
		self.register_accessory(Thermostat, "Thermostat", [device])

	def register_accessory(self, accessory_klass, name, devices):
		accessory = accessory_klass(self.driver, name, devices=devices)
		self.bridge.add_accessory(accessory)

	def connect(self):
		self.driver.add_accessory(accessory=self.bridge)
		signal.signal(signal.SIGTERM, self.driver.signal_handler)
		self.driver.start()


# mosquitto_pub -t "home/living/main/brightness/output" -m "54" -h 192.168.0.187
