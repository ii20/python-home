import requests
from datetime import datetime, timedelta

#          sunrise          sunset
#   00:00   07:00   12:00   18:00   00:00
#     10      5       0       5       10    <-- index at given time

class Daylight:
  def __init__(self):
    self.lat = "52.331128"
    self.lon = "5.540495"
    self.curDate = datetime.utcnow()
    self.fetchTimestamps()
    self.update()

  @property
  def value(self):
    if (datetime.utcnow() - self.curDate).total_seconds() > 120:
      self.update()

    return self._value

  def fetchTimestamps(self):
    url = "http://api.sunrise-sunset.org/json?formatted=0&lat="+self.lat+"&lng="+self.lon+"&date="+self.curDate.strftime("%Y-%m-%d")

    req = requests.get(url)
    res = req.json()['results']

    self.sunset  = datetime.strptime(res['sunset'], "%Y-%m-%dT%H:%M:%S+00:00")
    self.sunrise = datetime.strptime(res['sunrise'], "%Y-%m-%dT%H:%M:%S+00:00")

    # print(str(datetime.utcnow()))
    # print(str(self.sunrise))
    # print(str(self.sunset))

  def update(self):
    self.curDate = datetime.utcnow()

    if (self.curDate.day != self.sunset.day):
      self.fetchTimestamps()

    if self.curDate.hour > 12:
      diff1 = (self.curDate - self.sunset).total_seconds()
    else:
      diff1 = (self.sunrise - self.curDate).total_seconds()

    diff2 = divmod(diff1, 60)[0] + 50
    diff2 = max([diff2, 0])
    diff2 = min([diff2, 100])
    self._value = int(diff2 / 10)

# d=Daylight()
# print(d.value)
