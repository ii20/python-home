class ToggleLight:
  def setFull(device, index):
    device.setOn(1)

  def set(device, index, strength):
    on = ToggleLight.calcOn(device, index, strength)
    device.setOn(on)

  def calcOn(device, index, strength):
    newIndex = int(round(float(index) * strength))
    return 1 if newIndex > device.offset else 0


class FadeLight:
  def setFull(device, index):
    device.setBrightness(100)
    t = FadeLight.calcTemperature(device, index, 1)
    device.setTemperature(t)

  def set(device, index, strength):
    b = FadeLight.calcBrightness(device, index, strength)
    device.setBrightness(b)

    t = FadeLight.calcTemperature(device, index, strength)
    device.setTemperature(t)

  def calcBrightness(device, index, strength):
    if index < device.offset:
      return 0
    else:
      b = device.brightness_day - ((device.brightness_day - device.brightness_night) / (10 - device.offset) * (index - device.offset))
      return int(b * strength)

  def calcTemperature(device, index, strength):
    # (100 - 60) = 40 / 10 = 4 * 5 = 20
    t = ((device.temperature_day - device.temperature_night) / (10 - device.offset)) * (index - device.offset)
    return device.temperature_day - int(t)
