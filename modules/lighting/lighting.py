from modules.lighting.motion import Motion

class Lighting:
  MODES = {
    'OFF':    0.0,
    'SLEEP':  0.2,
    'RELAX':  0.6,
    'ACTIVE': 1.0,
    'FULL':   1.0
  }

  def __init__(self, location_key):
    self.location_key = location_key
    self.auto_on = False
    self.lights = []
    self.index = 0
    self.motion = Motion(self)
    self.mode = 'OFF'
    self.manual = False

    self.input_topic = '/'.join(['home', self.location_key, 'mode', 'input'])
    self.output_topic = '/'.join(['home', self.location_key, 'mode', 'output'])

    self.subscription = mqtt.add_subscription(self.input_topic, self.trigger)

  def unsubscribe(self):
    mqtt.remove_subscription(self.subscription)
    self.motion.unsubscribe()

  def __del__(self):
    print("Lighting removed____________________________________")

  def setMotion(self, motion_enabled, motion_trigger, motion_brightness, timeout_enabled, timeout_trigger, timeout_period):
    self.motion.motion_enabled = motion_enabled
    self.motion.motion_trigger = motion_trigger
    self.motion.motion_brightness = motion_brightness
    self.motion.timeout_enabled = timeout_enabled
    self.motion.timeout_trigger = timeout_trigger
    self.motion.timeout_period = timeout_period

  def addLight(self, device):
    klass = device.__class__.LightingKlass
    self.lights.append(klass(device))

  def trigger(self, value):
    if any(value in mode for mode in Lighting.MODES.keys()):
      self.manual = False
      self.mode = value
      self.motion.modeTrigger()
      self.update(self.index)
      mqtt.publish(self.output_topic, self.mode, retain=True)

  def update(self, new_index):
    self.index = new_index

    if self.manual:
      return

    for device in devices.withLocationKey(self.location_key):
      if self.mode == 'FULL':
        device.__class__.LightingKlass.setFull(device, self.index)
      else:
        device.__class__.LightingKlass.set(device, self.index, Lighting.MODES[self.mode])

  def setManual(self, value):
    self.manual = True
    self.motion.setTimeoutTimer(0)
    mqtt.publish(self.output_topic, 'OFF', True)

    strength = int(value) / 100.0

    for device in devices.withLocationKey(self.location_key):
      device.__class__.LightingKlass.set(device, self.index, strength)

  def getModeIndex(self, mode):
    return list(Lighting.MODES.keys()).index(mode)

