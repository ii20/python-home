from threading import Timer

class Motion:
  def __init__(self, lighting):
    self.lighting = lighting

    self.motion_enabled    = False
    self.motion_trigger    = 'OFF'
    self.motion_brightness = 0
    self.timeout_enabled   = False
    self.timeout_trigger   = 'OFF'
    self.timeout_period    = 0

    self.setBrightness(0)

    self.timer1 = Timer(0, None) # When motion trigger, turn lights off after 2min
    self.timer2 = Timer(0, None) # When trigger, ignore additional triggers for 1min

    self.topic1 = '/'.join(['home', lighting.location_key, 'wallswitch', 'pir', 'output'])
    self.subscription1 = mqtt.add_subscription(self.topic1, self.motionTrigger)

    self.topic2 = '/'.join(['home', lighting.location_key, 'wallswitch', 'brightness', 'output'])
    self.subscription2 = mqtt.add_subscription(self.topic2, self.setBrightness)

    self.topic3 = '/'.join(['home', lighting.location_key, 'lighting', 'timeout', 'input'])
    self.subscription3 = mqtt.add_subscription(self.topic3, self.setTimeoutTimer)

  def unsubscribe(self):
    mqtt.remove_subscription(self.subscription1)
    mqtt.remove_subscription(self.subscription2)
    mqtt.remove_subscription(self.subscription3)
    self.lighting = None

  def __del__(self):
    print("Motion removed____________________________________")

  def setBrightness(self, value):
    self.current_brightness = int(value)

  def motionTrigger(self, value):
    if not self.motion_enabled:
      return

    if not self.timer2.is_alive() and self.motionTriggerConditions():
      self.lighting.trigger('SLEEP' if home.modeEquals('NIGHT') else self.motion_trigger)

    if self.timer1.is_alive():
      self.setTimeoutTimer(self.timeout_period)

  def motionTriggerConditions(self):
    if self.did_timeout and self.getTimeoutTrigger() != 'OFF' and self.lighting.mode == self.getTimeoutTrigger():
      return True

    if self.lighting.mode == 'OFF' and (self.motion_brightness > 0 and self.current_brightness < self.motion_brightness):
      return True

    return False

  def modeTrigger(self):
    # if current mode greater than timeout value
    if (self.lighting.getModeIndex(self.lighting.mode) > self.lighting.getModeIndex(self.getTimeoutTrigger())):
      self.setTimeoutTimer(self.timeout_period)
    else:
      self.setTimeoutTimer(0)

    self.did_timeout = False
    self.setIgnoreTimer()

  def setIgnoreTimer(self):
    self.timer2.cancel()
    self.timer2 = Timer(60.0, lambda: None)
    self.timer2.start()

  def setTimeoutTimer(self, period=0):
    if self.timeout_enabled:
      self.timer1.cancel()
      mqtt.publish('home/'+self.lighting.location_key+'/lighting/timeout/output', str(period))

      if int(period) > 0:
        self.timer1 = Timer(int(period), self.setTimeoutTrigger)
        self.timer1.start()

  def setTimeoutTrigger(self):
    self.lighting.trigger(self.getTimeoutTrigger())
    self.did_timeout = True

  def getTimeoutTrigger(self):
    return 'OFF' if home.modeEquals('NIGHT') else self.timeout_trigger
