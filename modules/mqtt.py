import uuid
import requests
import paho.mqtt.client as mqtt

class Mqtt:
  def __init__(self, host):
    self.host = host
    self.client = mqtt.Client()
    self.client.on_disconnect = self.on_disconnect
    self.client.on_connect = self.on_connect
    self.client.on_message = self.on_message

    self.connected = False
    self.subscriptions = []

  def on_disconnect(self, client, userdata, rc):
    self.connected = False

  def on_connect(self, client, userdata, flags, rc):
    self.connected = True

    for sub in self.subscriptions:
      client.subscribe(sub['topic'])
      print(" - " + sub['topic'])

  def on_message(self, client, userdata, msg):
    self.trigger_subscriptions(msg.topic, msg.payload.decode("utf-8"))

  def publish(self, topic, message, retain=False, qos=0):
    self.client.publish(topic, message, retain=retain, qos=qos)

  def add_subscription(self, topic, callback):
    new_subscription = {
      'id': str(uuid.uuid4()),
      'topic': topic,
      'callback': callback
    }

    if self.connected and not self.subscription_exists(topic):
      self.client.subscribe(topic)

    self.subscriptions.append(new_subscription)
    return new_subscription['id']

  def remove_subscription(self, subscription_id):
    for index in range(len(self.subscriptions)):
      if self.subscriptions[index]['id'] == subscription_id:
        topic = self.subscriptions[index]['topic']
        self.subscriptions.pop(index)

        if self.connected and not self.subscription_exists(topic):
          self.client.unsubscribe(topic)

        return True

    return False

  def connect(self):
    self.client.connect(self.host, 1883, 60)
    self.client.loop_start()
    # self.client.loop_forever()

  def disconnect(self):
    self.client.loop_stop()

  def trigger_subscriptions(self, topic, message):
    for sub in self.subscriptions:
      if mqtt.topic_matches_sub(sub['topic'], topic):
        sub['callback'](message)

  def subscription_exists(self, topic):
    for sub in self.subscriptions:
      if sub['topic'] == topic:
        return True

    return False

