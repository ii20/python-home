import json
import shelve

class BaseCrud:
  def __init__(self, type):
    self.type = type
    self.list = self.getStore()
    print("Initialize CRUD for " + self.type + " with " + str(len(self.list)) + " items.")

    mqtt.add_subscription("home/config/"+self.type+"/list/input", self.requestList)
    mqtt.add_subscription("home/config/"+self.type+"/create/input", self.requestCreate)
    mqtt.add_subscription("home/config/"+self.type+"/update/input", self.requestUpdate)
    mqtt.add_subscription("home/config/"+self.type+"/delete/input", self.requestDelete)

  def requestList(self, value=None):
    mqtt.publish("home/config/"+self.type+"/list/output", json.dumps(self.getList(), ensure_ascii=False))

  def requestCreate(self, value):
    self.createInstance(json.loads(value))
    self.requestList()

  def requestUpdate(self, value):
    self.updateInstance(json.loads(value))
    self.requestList()

  def requestDelete(self, value):
    self.deleteInstance(json.loads(value))
    self.requestList()

  def createInstance(self, attributes):
    new_instance = self.getInstanceKlass(attributes).create(attributes)
    self.list.append(new_instance)
    self.setStore()

  def updateInstance(self, attributes):
    for instance in self.list:
      if instance.id == attributes["id"]:
        instance.update(attributes)
        self.setStore()

  def deleteInstance(self, attributes):
    for index in range(len(self.list)):
      if self.list[index].id == attributes["id"]:
        self.list.pop(index)
        # del self.list[index]
        self.setStore()

  def getList(self):
    return [instance.toAttributes() for instance in self.list]

  def getStore(self):
    with shelve.open('store') as store:
      if self.type in store:
        return [self.getInstanceKlass(attributes)(**attributes) for attributes in store[self.type]]
      else:
        return []

  def setStore(self):
    store = shelve.open('store')
    store[self.type] = self.getList()
    store.close()
