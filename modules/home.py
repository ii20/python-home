import time
from datetime import datetime
from threading import Timer, Thread

from modules.lighting.daylight import Daylight

class Home:
  MODES = ['ACTIVE', 'NIGHT', 'AWAY']

  def __init__(self):
    self.mode = 'ACTIVE'
    self.index = 0
    self.thread = Thread(target=self.loop)
    self.daylight = Daylight()

    self.motion_timer = Timer(0, None) # When motion trigger, cancel triggers for 60s
    self.night_timer = Timer(0, None) # When night is activated, turn off all lights after 150s

    mqtt.add_subscription('home/mode/input', self.modeTrigger)
    mqtt.add_subscription('home/+/+/pir/output', self.motionTrigger)

  def modeTrigger(self, value):
    if any(value in mode for mode in Home.MODES):
      old_mode = self.mode
      self.mode = value

      self.startMotionTimer()

      if (self.mode == 'NIGHT'):
        self.setNightMode()

      if (self.mode == 'AWAY'):
        self.setLightingOff();

      if (self.mode == 'ACTIVE' and self.index > 0):
        self.setAutoOn()

      print("Set home mode to " + self.mode)
      mqtt.publish('home/mode/output', self.mode, retain=True)

  # motion trigger on one of the wallswitches
  def motionTrigger(self, value):
    if (self.mode == 'AWAY') and not self.motion_timer.is_alive():
      self.modeTrigger('ACTIVE')

    if (self.mode == 'NIGHT' and self.night_timer.is_alive()):
      self.setNightMode()

  def update(self):
    old_index = self.index
    self.index = self.daylight.value

    if self.mode == 'ACTIVE' and old_index == 0 and self.index == 1:
      self.setAutoOn()

    if self.mode == 'NIGHT' and datetime.now().hour == 10:
      self.modeTrigger('ACTIVE')

    for location in locations.withLighting():
      location.lighting_instance.update(self.index)

  def start(self):
    self.thread.start()

  def loop(self):
    while(True):
      self.update()
      time.sleep(60)

  def setNightMode(self):
    self.night_timer.cancel()
    self.night_timer = Timer(150.0, self.setLightingOff)
    self.night_timer.start()

    for location in locations.withLighting():
      if location.lighting_instance.mode != 'OFF':
        location.lighting_instance.trigger('SLEEP')
        location.lighting_instance.motion.setTimeoutTimer(0)

  def setLightingOff(self):
    for location in locations.withLighting():
      location.lighting_instance.trigger('OFF')

  def setAutoOn(self):
    for location in locations.withLightingAutoOn():
      if location.lighting_instance.mode == 'OFF':
        location.lighting_instance.trigger('ACTIVE')

  def startMotionTimer(self):
    self.motion_timer.cancel()
    self.motion_timer = Timer(60.0, lambda: None)
    self.motion_timer.start()

  def modeEquals(self, mode):
    return self.mode == mode
