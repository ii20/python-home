import uuid
import json

from modules.devices.base import Base
from modules.lighting.light import FadeLight

class Zigbee(Base):
  DEVICE_TYPE = 'zigbee'

  BRIGHTNESS_DAY_VALUE = 100
  BRIGHTNESS_NIGHT_VALUE = 60

  TEMPERATURE_DAY_VALUE = 70
  TEMPERATURE_NIGHT_VALUE = 100

  LightingKlass = FadeLight

  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)

    self.offset = kwargs.get('offset') or 0
    self.brightness_day = kwargs.get("brightness_day") or Zigbee.BRIGHTNESS_DAY_VALUE
    self.brightness_night = kwargs.get("brightness_night") or Zigbee.BRIGHTNESS_NIGHT_VALUE
    self.temperature_day = kwargs.get("temperature_day") or Zigbee.TEMPERATURE_DAY_VALUE
    self.temperature_night = kwargs.get("temperature_night") or Zigbee.TEMPERATURE_NIGHT_VALUE
    self.device_id = kwargs.get('device_id')

    self._brightness = 0
    self._temperature = 0

  @staticmethod
  def create(attributes):
    attributes["id"] = str(uuid.uuid4())
    return Zigbee(**attributes)

  def setBrightness(self, value):
    self._brightness = value
    # mqtt.publish("zigbee2mqtt/"+self.device_id+"/set", "{\"state\":\"ON\", \"brightness\": "+ str(int(self._brightness*2.55)) +"}")

    # mosquitto_pub -h 192.168.0.129 -t "zigbee/0x00158d0002e8f838/set" -m "{\"state\": \"ON\", \"color_temp\": \"0\" }"

  def setTemperature(self, value):
    self._temperature = value

  def update(self, attributes):
    super(Zigbee, self).update(attributes)
    self.device_id = attributes.get("device_id")
    self.brightness_day = attributes.get("brightness_day") or Zigbee.BRIGHTNESS_DAY_VALUE
    self.brightness_night = attributes.get("brightness_night") or Zigbee.BRIGHTNESS_NIGHT_VALUE
    self.temperature_day = attributes.get("temperature_day") or Zigbee.TEMPERATURE_DAY_VALUE
    self.temperature_night = attributes.get("temperature_night") or Zigbee.TEMPERATURE_NIGHT_VALUE

  def toAttributes(self):
    attrs = super(Zigbee, self).toAttributes()
    attrs.update({
      "device_id": self.device_id,
      "brightness_day": self.brightness_day,
      "brightness_night": self.brightness_night,
      "temperature_day": self.temperature_day,
      "temperature_night": self.temperature_night
    })
    return attrs
