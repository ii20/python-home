import uuid
import json

from modules.devices.base import Base
from modules.lighting.light import ToggleLight

class Switch(Base):
  DEVICE_TYPE = 'switch'

  LightingKlass = ToggleLight

  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)

    self.topic = "home/"+self.location_key+"/switch/on/input"
    self.offset = kwargs.get('offset') or 0

  @staticmethod
  def create(attributes):
    attributes["id"] = str(uuid.uuid4())
    return Switch(**attributes)

  def setOn(self, value):
    mqtt.publish(self.topic, str(value))
