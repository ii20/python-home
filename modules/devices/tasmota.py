import uuid
import json

from modules.devices.base import Base
from modules.lighting.light import FadeLight

class Tasmota(Base):
  DEVICE_TYPE = 'tasmota'

  BRIGHTNESS_DAY_VALUE = 100
  BRIGHTNESS_NIGHT_VALUE = 60

  TEMPERATURE_DAY_VALUE = 70
  TEMPERATURE_NIGHT_VALUE = 100

  LightingKlass = FadeLight

  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)

    self.offset = kwargs.get('offset') or 0
    self.brightness_day = kwargs.get("brightness_day") or Tasmota.BRIGHTNESS_DAY_VALUE
    self.brightness_night = kwargs.get("brightness_night") or Tasmota.BRIGHTNESS_NIGHT_VALUE
    self.temperature_day = kwargs.get("temperature_day") or Tasmota.TEMPERATURE_DAY_VALUE
    self.temperature_night = kwargs.get("temperature_night") or Tasmota.TEMPERATURE_NIGHT_VALUE

    self._brightness = 0
    self._temperature = 0

  @staticmethod
  def create(attributes):
    attributes["id"] = str(uuid.uuid4())
    return Tasmota(**attributes)

  def setBrightness(self, value):
    self._brightness = value
    self.updateLight()

  def setTemperature(self, value):
    self._temperature = value
    self.updateLight()

  def updateLight(self):
    channel1 = int(self._brightness * (100-self._temperature) / 100.0)
    channel2 = int(self._brightness * self._temperature / 100.0)

    if (channel1 + channel2) > 0 and (channel1 + channel2) < 30:
      channel1 += 8
      channel2 += 8

    mqtt.publish("cmnd/"+self.location_key+"/"+self.key()+"/Channel1", channel1)
    mqtt.publish("cmnd/"+self.location_key+"/"+self.key()+"/Channel2", channel2)

  def update(self, attributes):
    super(Tasmota, self).update(attributes)
    self.brightness_day = attributes.get("brightness_day") or Tasmota.BRIGHTNESS_DAY_VALUE
    self.brightness_night = attributes.get("brightness_night") or Tasmota.BRIGHTNESS_NIGHT_VALUE
    self.temperature_day = attributes.get("temperature_day") or Tasmota.TEMPERATURE_DAY_VALUE
    self.temperature_night = attributes.get("temperature_night") or Tasmota.TEMPERATURE_NIGHT_VALUE

  def toAttributes(self):
    attrs = super(Tasmota, self).toAttributes()
    attrs.update({
      "brightness_day": self.brightness_day,
      "brightness_night": self.brightness_night,
      "temperature_day": self.temperature_day,
      "temperature_night": self.temperature_night
    })
    return attrs

  def key(self):
    return self.name.replace(" ", "").lower()
