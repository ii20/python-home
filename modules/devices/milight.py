import uuid
import json

from modules.devices.base import Base
from modules.lighting.light import FadeLight

class Milight(Base):
  DEVICE_TYPE = 'milight'

  BRIGHTNESS_DAY_VALUE = 100
  BRIGHTNESS_NIGHT_VALUE = 60

  TEMPERATURE_DAY_VALUE = 70
  TEMPERATURE_NIGHT_VALUE = 100

  LightingKlass = FadeLight

  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)

    self.offset = kwargs.get('offset') or 0
    self.brightness_day = kwargs.get("brightness_day") or Milight.BRIGHTNESS_DAY_VALUE
    self.brightness_night = kwargs.get("brightness_night") or Milight.BRIGHTNESS_NIGHT_VALUE
    self.temperature_day = kwargs.get("temperature_day") or Milight.TEMPERATURE_DAY_VALUE
    self.temperature_night = kwargs.get("temperature_night") or Milight.TEMPERATURE_NIGHT_VALUE

    self.topic = "home/milight/update/input"
    self.device_id = kwargs.get('device_id')

  @staticmethod
  def create(attributes):
    attributes["id"] = str(uuid.uuid4())
    return Milight(**attributes)

  def setBrightness(self, value):
    mqtt.publish(self.topic, json.dumps({ "device_ids": str(self.device_id), "level": value }, ensure_ascii=False))

  def setTemperature(self, value):
    mqtt.publish(self.topic, json.dumps({ "device_ids": str(self.device_id), "temperature": value }, ensure_ascii=False))

  def update(self, attributes):
    super(Milight, self).update(attributes)
    self.device_id = attributes.get("device_id")
    self.brightness_day = attributes.get("brightness_day") or Milight.BRIGHTNESS_DAY_VALUE
    self.brightness_night = attributes.get("brightness_night") or Milight.BRIGHTNESS_NIGHT_VALUE
    self.temperature_day = attributes.get("temperature_day") or Milight.TEMPERATURE_DAY_VALUE
    self.temperature_night = attributes.get("temperature_night") or Milight.TEMPERATURE_NIGHT_VALUE

  def toAttributes(self):
    attrs = super(Milight, self).toAttributes()
    attrs.update({
      "device_id": self.device_id,
      "brightness_day": self.brightness_day,
      "brightness_night": self.brightness_night,
      "temperature_day": self.temperature_day,
      "temperature_night": self.temperature_night
    })
    return attrs
