from modules.base_crud import BaseCrud
from modules.devices.milight import Milight
from modules.devices.switch import Switch
from modules.devices.milight_controller import MilightController
from modules.devices.tasmota import Tasmota
from modules.devices.zigbee import Zigbee

class Devices(BaseCrud):
  DEVICE_LOOKUP = {
    Milight.DEVICE_TYPE: Milight,
    Switch.DEVICE_TYPE: Switch,
    MilightController.DEVICE_TYPE: MilightController,
    Tasmota.DEVICE_TYPE: Tasmota,
    Zigbee.DEVICE_TYPE: Zigbee
  }

  def __init__(self):
    super().__init__("devices")

  def withLocationKey(self, location_key):
    return [device for device in self.list if device.location_key == location_key]

  def getInstanceKlass(self, attributes):
    return Devices.DEVICE_LOOKUP[attributes.get('type')]
