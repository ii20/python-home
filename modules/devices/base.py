import uuid
import json

class Base:
  DEVICE_TYPE = 'base'

  def __init__(self, *args, **kwargs):
    self.id           = kwargs.get("id")
    self.type         = kwargs.get("type")
    self.name         = kwargs.get("name")
    self.location_key = kwargs.get("location_key")

  def update(self, attributes):
    self.name = attributes.get("name")
    self.location_key = attributes.get("location_key")

  @staticmethod
  def create(attributes):
    attributes["id"] = str(uuid.uuid4())
    return Base(**attributes)

  def toAttributes(self):
    return { "id": self.id, "type": self.__class__.DEVICE_TYPE, "name": self.name, "location_key": self.location_key }

  def toJson(self):
    return json.dumps(self.toAttributes(), ensure_ascii=False)

  def location(self):
    return locations.findByKey(self.location_key)
