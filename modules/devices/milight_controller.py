import uuid
import json

from modules.devices.base import Base
from modules.lighting.light import FadeLight

from limitlessled.group.rgbww import RGBWW

class MilightController(Base):
  DEVICE_TYPE = 'milight_controller'

  LightingKlass = FadeLight

  BRIGHTNESS_DAY_VALUE = 100
  BRIGHTNESS_NIGHT_VALUE = 60

  TEMPERATURE_DAY_VALUE = 70
  TEMPERATURE_NIGHT_VALUE = 100

  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)

    self.offset = kwargs.get('offset') or 0
    self.brightness_day = kwargs.get("brightness_day") or Milight.BRIGHTNESS_DAY_VALUE
    self.brightness_night = kwargs.get("brightness_night") or Milight.BRIGHTNESS_NIGHT_VALUE
    self.temperature_day = kwargs.get("temperature_day") or Milight.TEMPERATURE_DAY_VALUE
    self.temperature_night = kwargs.get("temperature_night") or Milight.TEMPERATURE_NIGHT_VALUE

    self.device_id = kwargs.get('device_id')
    self.bulb = bridge.add_group(self.device_id, str(self.device_id), RGBWW)

  @staticmethod
  def create(attributes):
    attributes["id"] = str(uuid.uuid4())
    return MilightController(**attributes)

  def setBrightness(self, value):
    b = int(value)
    if 0 <= b <= 100:
      if b > 0:
        self.turn(b)
        self.bulb.brightness = float(b/100)
      else:
        self.bulb.brightness = float(b/100)
        self.turn(b)

  def setTemperature(self, value):
    t = 100 - int(value)
    if 0 <= t <= 100:
      self.bulb.temperature = float(t/100)

  def update(self, attributes):
    super(MilightController, self).update(attributes)
    self.device_id = attributes.get("device_id")
    self.brightness_day = attributes.get("brightness_day") or Milight.BRIGHTNESS_DAY_VALUE
    self.brightness_night = attributes.get("brightness_night") or Milight.BRIGHTNESS_NIGHT_VALUE
    self.temperature_day = attributes.get("temperature_day") or Milight.TEMPERATURE_DAY_VALUE
    self.temperature_night = attributes.get("temperature_night") or Milight.TEMPERATURE_NIGHT_VALUE

  def toAttributes(self):
    attrs = super(MilightController, self).toAttributes()
    attrs.update({
      "device_id": self.device_id,
      "brightness_day": self.brightness_day,
      "brightness_night": self.brightness_night,
      "temperature_day": self.temperature_day,
      "temperature_night": self.temperature_night
    })
    return attrs

  def turn(self, v):
    self.bulb.on = bool(v)
