import requests
import json
import threading
import time

from modules.spotify_authorization import SpotifyAuthorization

class Spotify():
  def __init__(self):
    self.access = SpotifyAuthorization()
    self.thread = threading.Thread(target=self.loop)
    self.simple = False

    mqtt.add_subscription("home/spotify/authorize/input", self.auth_trigger)
    mqtt.add_subscription("home/spotify/context/input", self.context_trigger)
    mqtt.add_subscription("home/spotify/control/input", self.control_trigger)
    mqtt.add_subscription("home/spotify/devices/input", self.devices_trigger)
    mqtt.add_subscription("home/spotify/device/input", self.device_trigger)
    mqtt.add_subscription("home/spotify/simple/input", self.simple_trigger)

  def start(self):
    self.thread.start()

  def loop(self):
    while(True):
      if (self.access.accessible):
        self.player()
        time.sleep(3)
      else:
        self.publishError("Unauthorized")
        time.sleep(10)

  def auth_trigger(self, payload):
    self.access.authorizeCode(payload)
    self.player()

  def control_trigger(self, payload):
    self.control(payload)
    self.player()

  def context_trigger(self, payload):
    self.context(payload)
    self.player()

  def simple_trigger(self, payload):
    self.simple = (payload == '1')

  def devices_trigger(self, payload):
    url = "https://api.spotify.com/v1/me/player/devices"
    response = requests.get(url, headers=self.headers())

    if (response.status_code == 200):
      devices_json = json.dumps(response.json(), ensure_ascii=False)
      mqtt.publish('home/spotify/devices/output', devices_json)

  def device_trigger(self, payload):
    url = "https://api.spotify.com/v1/me/player"
    data = {
      'device_ids': [str(payload)],
      'play': True
    }

    response = requests.put(url, json=data, headers=self.headers())

  def player(self):
    url = "https://api.spotify.com/v1/me/player"

    try:
      response = requests.get(url, headers=self.headers())

      if (response.status_code == 200):
        self.publishPlayerInfo(response.json())
        if self.simple:
          self.publishSimplePlayerInfo(response.json())
      elif(response.status_code == 401):
        self.access.refreshToken()
      else:
        self.publishError(str(response.status_code))
    except requests.exceptions.ConnectionError:
      print("Too many spotify requests")
      self.publishError("Too many requests")


  def publishPlayerInfo(self, data):
    player_info = {
      'playing': data["is_playing"],
      'artist': ", ".join(list(map(lambda artist: artist['name'], data["item"]["artists"]))),
      'song': data["item"]["name"],
      'album': data["item"]["album"]["name"],
      'image': data["item"]["album"]["images"][0]["url"],
      'duration_total': data["item"]["duration_ms"],
      'duration_progress': data["progress_ms"],
      'device': {
        'id': data["device"]["id"],
        'name': data["device"]["name"],
        'volume': data["device"]["volume_percent"]
      }
    }

    player_info_json = json.dumps(player_info, ensure_ascii=False)
    mqtt.publish('home/spotify/player/output', player_info_json, retain=True)

  def publishSimplePlayerInfo(self, data):
    player_info = {
      'playing': data["is_playing"],
      'artist': ", ".join(list(map(lambda artist: artist['name'], data["item"]["artists"]))),
      'song': data["item"]["name"],
      'duration_total': data["item"]["duration_ms"],
      'duration_progress': data["progress_ms"]
    }

    player_info_json = json.dumps(player_info, ensure_ascii=False)
    mqtt.publish('home/spotify/simple/output', player_info_json, retain=True)

  def context(self, context):
    url = "https://api.spotify.com/v1/me/player/play"
    data = {
      'context_uri': context
    }

    response = requests.put(url, json=data, headers=self.headers())
    print(response)
    print(response.status_code)
    print(response.content)

  def control(self, action):
    if action == 'play':
      url = "https://api.spotify.com/v1/me/player/play"
      response = requests.put(url, data={}, headers=self.headers())

    if action == 'pause':
      url = "https://api.spotify.com/v1/me/player/pause"
      response = requests.put(url, data={}, headers=self.headers())

    if action == 'next':
      url = "https://api.spotify.com/v1/me/player/next"
      response = requests.post(url, data={}, headers=self.headers())

    if action == 'previous':
      url = "https://api.spotify.com/v1/me/player/previous"
      response = requests.post(url, data={}, headers=self.headers())


  def publishError(self, message):
    mqtt.publish('home/spotify/player/output', '{"error": "' + message + '"}')

  def headers(self):
    return {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer {0}'.format(self.access.accessToken())
    }
