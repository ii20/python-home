import uuid
import json

from modules.lighting.lighting import Lighting

class Location:
  def __init__(self, *args, **kwargs):
    self.id       = kwargs.get("id")
    self.key      = kwargs.get("key")
    self.name     = kwargs.get("name")
    self.lighting = kwargs.get("lighting") or False
    self.auto_on  = kwargs.get("auto_on") or False

    self.motion_enabled    = kwargs.get("motion_enabled") or False
    self.motion_trigger    = kwargs.get("motion_trigger") or 'OFF'
    self.motion_brightness = kwargs.get("motion_brightness") or 0

    self.timeout_enabled = kwargs.get("timeout_enabled") or False
    self.timeout_trigger = kwargs.get("timeout_trigger") or 'OFF'
    self.timeout_period  = kwargs.get("timeout_period") or 0

    self.lighting_instance = None
    self.initialize()

  def __del__(self):
    if self.lighting_instance:
      self.lighting_instance.unsubscribe()
    print("Location removed____________________________________")

  def initialize(self):
    if self.lighting and not self.lighting_instance:
      self.lighting_instance = Lighting(self.key)

    if not self.lighting and self.lighting_instance:
      self.lighting_instance.unsubscribe()
      self.lighting_instance = None

    if self.lighting_instance:
      self.lighting_instance.auto_on = self.auto_on
      self.lighting_instance.setMotion(self.motion_enabled, self.motion_trigger, self.motion_brightness, self.timeout_enabled, self.timeout_trigger, self.timeout_period)

  def update(self, attributes):
    self.name = attributes.get("name")
    self.lighting = attributes.get("lighting")
    self.auto_on = attributes.get("auto_on")
    self.motion_enabled = attributes.get("motion_enabled")
    self.motion_trigger = attributes.get("motion_trigger")
    self.motion_brightness = attributes.get("motion_brightness")
    self.timeout_enabled = attributes.get("timeout_enabled")
    self.timeout_trigger = attributes.get("timeout_trigger")
    self.timeout_period = attributes.get("timeout_period")
    self.initialize()

  @staticmethod
  def create(attributes):
    attributes["id"]  = str(uuid.uuid4())
    attributes["key"] = attributes["name"].replace(" ", "").lower()
    return Location(**attributes)

  def toAttributes(self):
    return {
      "id": self.id,
      "key": self.key,
      "name": self.name,
      "lighting": self.lighting,
      "auto_on": self.auto_on,
      "motion_enabled": self.motion_enabled,
      "motion_trigger": self.motion_trigger,
      "motion_brightness": self.motion_brightness,
      "timeout_enabled": self.timeout_enabled,
      "timeout_trigger": self.timeout_trigger,
      "timeout_period": self.timeout_period
    }

  def toJson(self):
    return json.dumps(self.toAttributes(), ensure_ascii=False)
