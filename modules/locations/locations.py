from modules.base_crud import BaseCrud
from modules.locations.location import Location

class Locations(BaseCrud):
  def __init__(self):
    super().__init__("locations")

  def withLighting(self):
    return [location for location in self.list if location.lighting == True]

  def withKey(self, key):
    return [location for location in self.list if location.key == key]

  def withLightingAutoOn(self):
    return [location for location in self.list if location.lighting == True and location.auto_on]

  def getInstanceKlass(self, attributes):
    return Location
