import base64
import requests
import shelve
from threading import Timer

class SpotifyAuthorization:
  def __init__(self):
    self.endpoint      = 'https://accounts.spotify.com/api/token'
    self.client_id     = 'eda6b0637eab41ddb93cae1bf52dfb76'
    self.client_secret = 'df51bbdc12ed4485a8360117e7bc5895'
    self.refresh_token = None
    self.access_token  = None
    self.timer         = None
    self.accessible    = False

    with shelve.open('store') as store:
      if 'refresh_token' in store:
        self.refresh_token = store['refresh_token']
        self.refreshToken()

  def accessToken(self):
    return self.access_token

  def authorizeCode(self, payload):
    code, redirect_uri = str(payload).split(';')

    payload = {
      'code': code,
      'redirect_uri': redirect_uri,
      'grant_type': 'authorization_code'
    }

    print("Get access token")

    response = requests.post(self.endpoint, data=payload, headers=self.headers())

    if (response.status_code == 200):
      self.access_token = response.json()['access_token']
      self.accessible   = True
      self.setRefreshToken(response.json()['refresh_token'])
      self.startRefreshTokenTimer()
      return 1
    else:
      self.accessible = False
      print("ERROR " + response.status_code + ": " + response.json())
      return 0

  def setRefreshToken(self, token):
    self.refresh_token = token

    store = shelve.open('store')
    store['refresh_token'] = self.refresh_token
    store.close()

  def refreshToken(self):
    if (self.refresh_token is None):
      return 'no_refresh_token'

    payload = {
      'refresh_token': self.refresh_token,
      'grant_type': 'refresh_token'
    }

    response = requests.post(self.endpoint, data=payload, headers=self.headers())

    if (response.status_code == 200):
      self.access_token = response.json()['access_token']
      self.accessible   = True
      self.startRefreshTokenTimer()
    else:
      self.accessible = False

  def startRefreshTokenTimer(self):
    # self.timer.cancel()
    self.timer = Timer(3000.0, self.refreshToken)
    self.timer.start()

  def headers(self):
    token = base64.b64encode(bytes(self.client_id + ':' + self.client_secret, 'utf-8')).decode("utf-8")
    return { 'Authorization': 'Basic ' + token }
