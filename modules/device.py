class Device:
  def __init__(self, topic):
    self.topic     = topic
    self.value     = ""
    self.callbacks = []

    self.input_topic = self.topic + '/input'
    self.output_topic = self.topic + '/output'

    self.addCallback(self.printValue)

    mqtt.add_subscription(self.output_topic, self.update)
    
  def addCallback(self, cb):
    self.callbacks.append(cb)

  def update(self, payload):
    self.value = payload
    self.triggerCallbacks()

  def publish(self, value):
    mqtt.publish(self.input_topic, value)

  def triggerCallbacks(self):
    for callback in self.callbacks:
      callback()

  def printValue(self):
    print('--------------------------------------')
    print('Read from device ' + self.topic)
    print('value = ' + self.value)
    print('--------------------------------------')
    print('')
    
