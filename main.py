from modules.device import Device
from modules.mqtt import Mqtt
from modules.homekit import Homekit
from modules.spotify import Spotify
from modules.home import Home

from modules.locations.locations import Locations
from modules.devices.devices import Devices

from limitlessled.bridge import Bridge

import os
import importlib
import builtins

MILIGHT_WIFI_CONTROLLER_IP = '192.168.0.191'

builtins.mqtt      = Mqtt('192.168.0.129')
builtins.bridge    = Bridge(MILIGHT_WIFI_CONTROLLER_IP)
builtins.devices   = Devices()
builtins.locations = Locations()
builtins.home      = Home()

spotify = Spotify()
homekit = Homekit()

for filename in os.listdir("flows"):
  if filename.endswith(".py") and not filename == '__init__.py':
    klassName = filename.split(".")[0]
    klassNameCamelCase = "".join(x.title() for x in klassName.split('_'))
    klass = getattr(__import__("flows."+klassName, fromlist=[klassName]), klassNameCamelCase)
    klass()

home.start()
spotify.start()
mqtt.connect()
homekit.connect() # This one is blocking

# binding.pry
# import code; code.interact(local=dict(globals(), **locals()))
