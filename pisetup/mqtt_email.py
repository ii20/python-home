#!/usr/bin/python3
# install aiosmtpd with:
# pip install aiosmtpd
# generate key for TLS:
# openssl req -x509 -newkey rsa:4096 -keyout key.pem -out cert.pem -days 999 -nodes -subj '/CN=localhost'

import asyncio
import logging
import requests
import ssl
import sys
import re
import paho.mqtt.publish as publish

from aiosmtpd.controller import Controller
from aiosmtpd.handlers import AsyncMessage
from aiosmtpd.smtp import SMTP as Server, syntax
from aiosmtpd.smtp import AuthResult

class MyController(Controller):
    @staticmethod
    def authenticator_func(server, session, envelope, mechanism, auth_data):
        return AuthResult(success=True)

    def factory(self):
        context = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
        context.load_cert_chain('cert.pem', 'key.pem')
        return Server(self.handler, tls_context=context, authenticator=self.authenticator_func)

class MyMessageHandler(AsyncMessage):
    @staticmethod
    def authenticator_func(server, session, envelope, mechanism, auth_data):
        return AuthResult(success=True)

# change to accept only emails for your domain here it is defined for: mycctv.local
    async def handle_RCPT(self, server, session, envelope, address, rcpt_options):
        if not address.endswith('@mycctv.local'):
            return '550 not relaying to that domain'
        envelope.rcpt_tos.append(address)
        return '250 OK'

    async def handle_DATA(self, server, session, envelope):
        sender = re.search(r".+?(?=\@)", envelope.mail_from)
        sender = sender.group()
        publish.single('camera/{}/alert'.format(sender), payload="1", hostname="localhost", client_id="mqtt-email")
        return '250 Message accepted for delivery'

async def amain(loop):
    cont = MyController(MyMessageHandler(), hostname='', port=8025)
    cont.start()

async def snapshot(loop):
    while True:
        print("create snapshot")
        await asyncio.sleep(2)

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.create_task(amain(loop=loop))
    # loop.create_task(snapshot(loop=loop))
    print(".....Starting Job.....")
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass
