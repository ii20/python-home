from modules.device import Device

class PingFlow:
  def __init__(self):
    mqtt.add_subscription('home/ping/input', self.trigger)

  def trigger(self, payload):
    mqtt.publish('home/ping/output', payload)
