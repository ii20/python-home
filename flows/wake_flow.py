import time
from datetime import datetime
from threading import Timer, Thread

from modules.device import Device

class WakeFlow:
  def __init__(self):
    self.time_hh = -1
    self.time_mm = -1

    self.interval = 0
    self.active = False

    mqtt.add_subscription('home/wake/time/input', self.setTime)
    mqtt.add_subscription('home/wake/active/input', self.setActive)

    self.thread = Thread(target=self.loop)
    self.thread.start()

  def setTime(self, payload):
    arr = payload.split(":", 1)

    if (0 <= int(arr[0]) < 24) and (0 <= int(arr[1]) < 60):
      self.time_hh = int(arr[0])
      self.time_mm = int(arr[1])

    mqtt.publish('home/wake/time/output', str(self.time_hh) + ":" + str(self.time_mm), True)

  def setActive(self, active):
    self.active   = bool(int(active))
    self.interval = 0

    if not self.active:
      for location in locations.withKey('bedroom'):
        location.lighting_instance.trigger('OFF')

    mqtt.publish('home/wake/active/output', str(int(self.active)), True)

  def loop(self):
    while(True):
      if (datetime.now().hour == self.time_hh and datetime.now().minute == self.time_mm):
        self.setActive(True)

      if (self.active and self.interval < 100):
        self.interval += 1

        for location in locations.withKey('bedroom'):
          location.lighting_instance.setManual(self.interval)

      time.sleep(40)
